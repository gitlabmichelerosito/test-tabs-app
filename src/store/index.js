import Vue from "vue";
import Vuex from "vuex";
import filesToShow from "@/assets/argument.json";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    tabIndex: 0,
    lowerBound: 0,
    upperBound: 2,
    tabsList: [],
    filesList: [],
    tabCounter: 0
  },
  mutations: {
    INCREMENT_TAB_INDEX(state) {
      if (state.tabIndex < state.upperBound) state.tabIndex++;
    },
    DECREMENT_TAB_INDEX(state) {
      if (state.tabIndex > state.lowerBound) state.tabIndex--;
    },
    SYNCHRONIZE_TAB_INDEX(state, newIndex) {
      if (newIndex < state.upperBound + 1 && newIndex > state.lowerBound - 1) {
        state.tabIndex = newIndex;
      }
    },
    GET_SIDEBAR_FILES_LIST(state, filesToShow) {
      if (filesToShow !== {}) {
        state.filesList = filesToShow;
      }
    },
    OPEN_FROM_SIDEBAR(state, aFile) {
      state.tabsList.push(aFile);
    },
    INCREMENT_TAB_COUNTER(state) {
      state.tabCounter++;
    },
    SET_UPPER_BOUND(state, newUpperBound) {
      state.upperBound = newUpperBound;
    }
  },
  actions: {
    increment_tab_index({ commit }) {
      commit("INCREMENT_TAB_INDEX");
    },
    decrement_tab_index({ commit }) {
      commit("DECREMENT_TAB_INDEX");
    },
    synchronize_tab_index({ commit }, newIndex) {
      commit("SYNCHRONIZE_TAB_INDEX", newIndex);
    },
    get_sidebar_files_list({ commit }) {
      commit("GET_SIDEBAR_FILES_LIST", filesToShow);
    },
    open_from_sidebar({ state, commit, dispatch }, aFile) {
      let filesId = [];
      if (state.tabsList !== []) {
        state.tabsList.forEach(storedFile => {
          filesId.push(storedFile.id);
        });
      }
      if (filesId.includes(aFile.id)) {
        dispatch("synchronize_tab_index", aFile.tab);
      } else {
        aFile.tab = state.tabCounter;
        commit("INCREMENT_TAB_COUNTER");
        commit("OPEN_FROM_SIDEBAR", aFile);
        commit("SET_UPPER_BOUND", state.tabsList.length - 1);
      }
    }
  },
  getters: {
    tab_index(state) {
      return state.tabIndex;
    },
    tabs_list(state) {
      return state.tabsList;
    },
    files_list(state) {
      return state.filesList;
    }
  },
  modules: {}
});
